import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Dimensions, StyleSheet, View, Text, TouchableOpacity, FlatList, Image, ActivityIndicator } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

export default function AnimeDetail({ route, navigation }) {
    const { itemID } = route.params;
    const [anime, setAnime] = useState({});
    const [genre, setGenre] = useState([]);

    const getAnime = (id) => {
        axios.get(`https://api.jikan.moe/v3/anime/${id}/`)
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1)
                setAnime(data1)
                setGenre(data1.genres)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    useEffect(() => {
        getAnime(itemID)
    }, [])

    if (Object.keys(anime).length === 0) {
        return (
            <View style={styles.container}>
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={{ fontWeight: 'bold', fontSize: 24, alignSelf: 'center' }}>
                        {anime.title}
                    </Text>
                    <View style={styles.mainLayout}>
                        <View style={styles.leftDesc}>
                            <Image style={{ height: 255, width: 180, marginBottom: 5 }} source={{ uri: anime.image_url }} />
                        </View>
                        <View style={styles.rightDesc}>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Type : </Text>
                                <Text style={styles.descText}>{anime.type}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Source : </Text>
                                <Text style={styles.descText}>{anime.source}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Episodes : </Text>
                                <Text style={styles.descText}>{anime.episodes}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Status : </Text>
                                <Text style={styles.descText}>{anime.status}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Duration : </Text>
                                <Text style={styles.descText}>{anime.duration}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Rating : </Text>
                                <Text style={styles.descText}>{anime.rating}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Score : </Text>
                                <Text style={styles.descText}>{anime.score}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Members : </Text>
                                <Text style={styles.descText}>{anime.members}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Favorites : </Text>
                                <Text style={styles.descText}>{anime.favorites}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ marginHorizontal: 5, marginBottom: 5 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontWeight: 'bold' }}>Trailer URL : </Text>
                            <TouchableOpacity>
                                <Text style={{ color: 'blue', maxWidth: Dimensions.get('window').width }}>{anime.trailer_url}</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontWeight: 'bold', marginBottom: 3 }}>Genre : </Text>
                        <FlatList
                            horizontal
                            data={genre}
                            keyExtractor={(item, index) => `${item.id}-${index}`}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableOpacity style={styles.boxItem}>
                                        <Text style={{
                                            color: 'black', borderWidth: 1, borderColor: 'black'
                                        }}>{item.name}</Text>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                        <Text style={styles.descTitle}>Synopsis</Text>
                        <Text style={{ maxWidth: Dimensions.get('window').width, textAlign: 'justify' }}>{anime.synopsis}</Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    mainLayout: {
        flexDirection: 'row',
        paddingVertical: 5,
        borderTopColor: 'black',
        borderTopWidth: 1
    },
    miniLayout: {
        flexDirection: 'row',
        paddingVertical: 2
    },
    leftDesc: {
        flexDirection: 'column',
        padding: 5
    },
    rightDesc: {
        flexDirection: 'column',
        paddingLeft: 5
    },
    descTitle: {
        marginBottom: 5,
        fontWeight: 'bold'
    },
    descText: {
        maxWidth: 150,
    },
    boxItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
        marginBottom: 5
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})