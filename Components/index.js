import React from 'react'
import { StyleSheet } from 'react-native'
import { UserProvider } from './Context/userContext';


import Router from './Router'

export default function AppWithStore() {
    return (
        <UserProvider>
            <Router />
        </UserProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
