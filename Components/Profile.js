import React, { useContext, useState, useEffect } from 'react';
import { ActivityIndicator, StyleSheet, View, Text, TouchableOpacity, Alert, FlatList, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
import { UserContext } from './Context/userContext'
import axios from 'axios';

export default function AboutScreen({ navigation }) {

    const [user, setUser] = useContext(UserContext)

    const [profile, setProfile] = useState({});

    const [friends, setFriends] = useState([]);
    const [favAnime, setFavAnime] = useState([]);
    const [favManga, setFavManga] = useState([]);

    const getProfile = () => {
        axios.get(`https://api.jikan.moe/v3/user/${user}/`)
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1)
                setProfile(data1)
                setFavAnime(data1.favorites.anime)
                setFavManga(data1.favorites.manga)
            }).catch(err => {
                console.log('error: ', err)
                setProfile({ name: user })
            })
    }

    const getFriends = () => {
        axios.get(`https://api.jikan.moe/v3/user/${user}/friends`)
            .then(res => {
                const data1 = (res.data.friends)
                console.log('res: ', data1)
                setFriends(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    useEffect(() => {
        getProfile()
        getFriends()
    }, [])

    const Logout = () => {
        setUser("")

        navigation.navigate("LoginScreen")
        Alert.alert(
            'Logout Success !',
            'See you again next time',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                },
                { text: 'OK', onPress: () => console.log('OK Pressed') }
            ],
            { cancelable: true }
        )
    }

    if (Object.keys(profile).length === 0) {
        return (
            <View style={styles.container}>
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    } else if (Object.keys(profile).length === 1) {
        return (
            <View style={styles.container} >
                <ScrollView>
                    <View style={styles.mainDescription}>
                        <Text style={styles.pageTitle}>My Profile</Text>
                        <Ionicons name="person-circle-outline" size={180} color="black" />
                        <Text style={{ fontWeight: 'bold', fontSize: 24, borderBottomWidth: 1, borderBottomColor: 'black' }}>{user}</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Gender | Location</Text>
                    </View>
                    <View style={styles.activities}>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>10</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Animes watching</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>3</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Animes Completed</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>4</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Manga Reading</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>12</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Manga Completed</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.mainDescription}>
                        <TouchableOpacity style={styles.button} onPressIn={() => Logout()}>
                            <Text style={{ fontWeight: 'bold', color: 'white' }}>Logout</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingHorizontal: 45, paddingVertical: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>External Portfolio</Text>
                        <View style={styles.contentBox}>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-bitbucket" size={30} color="black" />
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@bitazam</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-github" size={30} color="black" />
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@gitlabazam</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-dribbble" size={30} color="black" />
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@dribbazam</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-instagram" size={30} color="black" />
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@instazam</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: 45, paddingVertical: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Contact</Text>
                        <View style={styles.contentBox}>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-linkedin" size={30} color="black" />
                                <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>@linkazam</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="mail-outline" size={30} color="black" />
                                <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>azam@mail.com</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-whatsapp" size={30} color="black" />
                                <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>088999222333</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.boxItem}>
                                <Ionicons name="logo-facebook" size={30} color="black" />
                                <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>azamfuadi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
    else {
        return (
            <View style={styles.container} >
                <ScrollView>
                    <View style={styles.mainDescription}>
                        <Text style={styles.pageTitle}>My Profile</Text>
                        <Image style={{ height: 170, width: 170, borderRadius: 25, marginBottom: 5 }} source={{ uri: profile.image_url }} />
                        <Text style={{ fontWeight: 'bold', fontSize: 24, borderBottomWidth: 1, borderBottomColor: 'black' }}>{user}</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{profile.gender} | {profile.location}</Text>
                    </View>
                    <View style={styles.activities}>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>{profile.anime_stats.watching}</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Animes watching</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>{profile.anime_stats.completed}</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Animes Completed</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>{profile.manga_stats.reading}</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Manga Reading</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listActivities}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>{profile.manga_stats.completed}</Text>
                            <Text style={{ fontSize: 12, color: 'darkblue' }}>Manga Completed</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.mainDescription}>
                        <TouchableOpacity style={styles.button} onPress={() => Logout()}>
                            <Text style={{ fontWeight: 'bold', color: 'white' }}>Logout</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Friends</Text>
                        <View style={styles.contentBox}>
                            {friends.length > 0 && (
                                <FlatList
                                    horizontal
                                    data={friends}
                                    keyExtractor={(item, index) => `${item.id}-${index}`}
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity style={styles.boxItem}>
                                                <Image style={{ height: 50, width: 50, borderRadius: 10, marginBottom: 2 }} source={{ uri: item.image_url }} />
                                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'black', marginBottom: 3, maxWidth: 130 }} numberOfLines={1}>{item.username}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            )}
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Favorite Anime</Text>
                        <View style={styles.contentBox}>
                            {favAnime.length > 0 && (
                                <FlatList
                                    horizontal
                                    data={favAnime}
                                    keyExtractor={(item, index) => `${item.id}-${index}`}
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity style={styles.boxItem} onPress={() => navigation.navigate('AnimeDetail', { itemID: item.mal_id })}>
                                                <Image style={{ height: 50, width: 50, borderRadius: 10, marginBottom: 2 }} source={{ uri: item.image_url }} />
                                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'black', marginBottom: 3, maxWidth: 130 }} numberOfLines={1}>{item.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            )}
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Favorite Manga</Text>
                        <View style={styles.contentBox}>
                            {favManga.length > 0 && (
                                <FlatList
                                    horizontal
                                    data={favManga}
                                    keyExtractor={(item, index) => `${item.id}-${index}`}
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity style={styles.boxItem} onPress={() => navigation.navigate('MangaDetail', { itemID: item.mal_id })}>
                                                <Image style={{ height: 50, width: 50, borderRadius: 10, marginBottom: 2 }} source={{ uri: item.image_url }} />
                                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'black', marginBottom: 3, maxWidth: 130 }} numberOfLines={1}>{item.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            )}
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 25,
        fontWeight: 'bold'
    },
    mainDescription: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    activities: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    listActivities: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    },
    button: {
        flexDirection: 'row',
        backgroundColor: 'darkblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    },
    contentBox: {
        flexDirection: 'row',
        backgroundColor: 'lightgray',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'black'
    },
    boxItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})