import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'

import LoginScreen from '../LoginScreen'
import RegisterScreen from '../RegisterScreen'
import UsernameLogin from '../UsernameLogin'
import HomeScreen from '../Home'
import ScheduleScreen from '../Schedule'
import AnimeDetail from '../AnimeDetail'
import MangaDetail from '../MangaDetail'
import ProfileScreen from '../Profile'
import SplashScreen from '../StartSplash'
import LoginSplash from '../LoginSplash'

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="SplashScreen"
                    component={SplashScreen}
                    options={() => ({
                        headerTitle: 'SanberAnime',
                        headerTitleAlign: 'center',
                        headerLeft: null
                    })}
                />
                <Stack.Screen
                    name="LoginScreen"
                    component={LoginScreen}
                    options={() => ({
                        headerTitle: 'SanberAnime',
                        headerTitleAlign: 'center',
                        headerLeft: null
                    })}
                />
                <Stack.Screen
                    name="UsernameLogin"
                    component={UsernameLogin}
                    options={() => ({
                        headerTitle: 'SanberAnime',
                        headerTitleAlign: 'center',
                        headerLeft: null
                    })}
                />
                <Stack.Screen
                    name="LoginSplash"
                    component={LoginSplash}
                    options={() => ({
                        headerTitle: 'SanberAnime',
                        headerTitleAlign: 'center',
                        headerLeft: null
                    })}
                />
                <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
                <Stack.Screen name="HomeScreen" component={HomeScreen} />
                <Stack.Screen
                    name="MainApp"
                    component={MainApp}
                    options={() => ({
                        headerTitle: 'SanberAnime',
                        headerTitleAlign: 'center',
                        headerLeft: null
                    })}
                />
                <Stack.Screen name="AnimeDetail" component={AnimeDetail} />
                <Stack.Screen name="MangaDetail" component={MangaDetail} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                    iconName = focused ? 'md-home' : 'md-home-outline';
                } else if (route.name === 'Schedule') {
                    iconName = focused ? 'md-calendar' : 'md-calendar-outline';
                } else if (route.name === 'Profile') {
                    iconName = focused ? 'md-person-circle' : 'md-person-circle-outline';
                } else if (route.name === 'ProjectScreen') {
                    iconName = focused ? 'md-list-circle' : 'md-list-circle-outline';
                } else if (route.name === 'SkillProject') {
                    iconName = focused ? 'md-code-slash' : 'md-code-slash-outline';
                } else if (route.name === 'Chat') {
                    iconName = focused ? 'md-chatbubbles' : 'md-chatbubbles-outline';
                }


                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: 'darkblue',
            inactiveTintColor: 'gray',
        }}
    >
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Schedule" component={ScheduleScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
)