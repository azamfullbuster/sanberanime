import React, { useContext, useState } from 'react';
import { Image, StyleSheet, View, Text, TextInput, Alert, TouchableOpacity } from 'react-native';
import { UserContext } from './Context/userContext'

export default function UsernameLogin({ navigation }) {

    const [, setUser] = useContext(UserContext)

    const [input, setInput] = useState("");

    const MyAnimeList = [
        'syntack', 'MaybeAPerson', 'Darkness_lord32', 'purplepinapples', 'mewey', 'aberuwu', 'Ayato-Hiragi', 'Pelramos', 'xsiN2k', 'Claeux',
        'MizoPro', 'Genjutsuhh', '45b16', 'te', 'Nekomata1037', 'sandshark', 'Rainoth', 'iBzOtaku', 'miraris', 'kunalmanik95', 'mohafiz', 'animeno',
        'Captione', 'ArtoriasMoreder', 'gondoltam', 'Demonix_Emperor', 'Brynnwell', 'Ervelan', 'chewieeeeee', 'AstolfoPlushh', 'OniNeko', 'Aky', 'Tamayata', 'blackcrossover',
        'Scrappy_De', 'UltimateDeath', 'Acellah', 'Naru-Gin', 'Sergey_', 'Elfezen', 'Bckro', '-Rosia', 'smolseok', 'josio', 'LyzChan', 'Slap', 'Kineta', 'Lolly-Pop', 'Mizore'
    ]

    const submit = () => {

        if (MyAnimeList.includes(input)) {
            setUser(input)
            setInput("")
            Alert.alert(
                'Login Success !',
                'Welcome to the Anime Family',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                    },
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: true }
            )

            navigation.navigate("LoginSplash")
        } else {
            console.log("Login gagal")
            setInput("")
            Alert.alert(
                'Login Failed !',
                'Username not existed in MyAnimeList',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                    },
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: true }
            )
            return
        }
    }

    return (
        <View style={styles.container}>
            <Image
                style={styles.imageLogo}
                source={require('./assets/logo.png')}
            />
            <Text style={styles.pageTitle}>LOGIN</Text>
            <View style={styles.form}>
                <Text>My AnimeList Username</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Input MyAnimeList Username"
                    value={input}
                    onChangeText={(value) => setInput(value)}
                />
            </View>
            <View style={styles.actionZone}>
                <TouchableOpacity
                    style={styles.button}
                    onPressIn={() => submit()}
                >
                    <Text style={{ fontWeight: 'bold', color: 'white' }}>Login</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    imageLogo: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 35,
        height: 78,
        width: 290
    },
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 65,
        fontSize: 25,
        fontWeight: 'bold'
    },
    form: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: 24,
        paddingVertical: 24,
    },
    input: {
        borderWidth: 1,
        borderColor: 'grey',
        paddingHorizontal: 10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    },
    actionZone: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: 24,
    },
    button: {
        flexDirection: 'row',
        backgroundColor: 'darkblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    },
    buttonGoogle: {
        flexDirection: 'row',
        backgroundColor: 'lightblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    }
})