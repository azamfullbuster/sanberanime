import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Moment from 'moment';
import { ActivityIndicator, FlatList, StyleSheet, View, Text, Image, Button, Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

export default function Home({ navigation }) {

    const [monday, setMonday] = useState([]);
    const [tuesday, setTuesday] = useState([]);
    const [wednesday, setWednesday] = useState([]);
    const [thursday, setThursday] = useState([]);
    const [friday, setFriday] = useState([]);

    const getMonday = () => {
        axios.get('https://api.jikan.moe/v3/schedule/monday')
            .then(res => {
                const data1 = (res.data.monday)
                console.log('res: ', data1)
                setMonday(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    const getTuesday = () => {
        axios.get('https://api.jikan.moe/v3/schedule/tuesday')
            .then(res => {
                const data1 = (res.data.tuesday)
                console.log('res: ', data1)
                setTuesday(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    const getWednesday = () => {
        axios.get('https://api.jikan.moe/v3/schedule/wednesday')
            .then(res => {
                const data1 = (res.data.wednesday)
                console.log('res: ', data1)
                setWednesday(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    const getThursday = () => {
        axios.get('https://api.jikan.moe/v3/schedule/thursday')
            .then(res => {
                const data1 = (res.data.thursday)
                console.log('res: ', data1)
                setThursday(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    const getFriday = () => {
        axios.get('https://api.jikan.moe/v3/schedule/friday')
            .then(res => {
                const data1 = (res.data.friday)
                console.log('res: ', data1)
                setFriday(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    useEffect(() => {
        getMonday()
        getTuesday()
        getWednesday()
        getThursday()
        getFriday()
    }, [])

    Moment.locale('en');

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: '1', title: 'Monday' },
        { key: '2', title: 'Tuesday' },
        { key: '3', title: 'Wednesday' },
        { key: '4', title: 'Thursday' },
        { key: '5', title: 'Friday' }
    ]);

    const Monday = () => {
        return (
            <View style={{ alignItems: 'center', marginBottom: 5 }}>
                <FlatList
                    data={monday}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.content}>
                                <Text style={{ fontWeight: 'bold', marginTop: 6, marginBottom: 5 }} numberOfLines={1}>{item.title}</Text>
                                <Image style={{ height: 170, width: 120, marginBottom: 5 }} source={{ uri: item.image_url }} />
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Type : </Text>
                                    <Text style={styles.descText}>{item.type}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Episode : </Text>
                                    <Text style={styles.descText}>{item.episodes}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Airing : </Text>
                                    <Text style={styles.descText}>{Moment(item.airing_start).format('d MMM')}</Text>
                                </View>
                                <Button
                                    onPress={() => navigation.navigate('AnimeDetail', { itemID: item.mal_id })}
                                    title="Detail"
                                />
                            </View>
                        )
                    }}
                />
            </View>
        )
    }

    const Tuesday = () => {
        return (
            <View style={{ alignItems: 'center', marginBottom: 5 }}>
                <FlatList
                    data={tuesday}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.content}>
                                <Text style={{ fontWeight: 'bold', marginTop: 6, marginBottom: 5 }} numberOfLines={1}>{item.title}</Text>
                                <Image style={{ height: 170, width: 120, marginBottom: 5 }} source={{ uri: item.image_url }} />
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Type : </Text>
                                    <Text style={styles.descText}>{item.type}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Episode : </Text>
                                    <Text style={styles.descText}>{item.episodes}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Airing : </Text>
                                    <Text style={styles.descText}>{Moment(item.airing_start).format('d MMM')}</Text>
                                </View>
                                <Button
                                    onPress={() => navigation.navigate('AnimeDetail', { animeID: item.mal_id })}
                                    title="Detail"
                                />
                            </View>
                        )
                    }}
                />
            </View>
        )
    }

    const Wednesday = () => {
        return (
            <View style={{ alignItems: 'center', marginBottom: 5 }}>
                <FlatList
                    data={wednesday}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.content}>
                                <Text style={{ fontWeight: 'bold', marginTop: 6, marginBottom: 5 }} numberOfLines={1}>{item.title}</Text>
                                <Image style={{ height: 170, width: 120, marginBottom: 5 }} source={{ uri: item.image_url }} />
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Type : </Text>
                                    <Text style={styles.descText}>{item.type}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Episode : </Text>
                                    <Text style={styles.descText}>{item.episodes}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Airing : </Text>
                                    <Text style={styles.descText}>{Moment(item.airing_start).format('d MMM')}</Text>
                                </View>
                                <Button
                                    onPress={() => navigation.navigate('AnimeDetail', { animeID: item.mal_id })}
                                    title="Detail"
                                />
                            </View>
                        )
                    }}
                />
            </View>
        )
    }

    const Thursday = () => {
        return (
            <View style={{ alignItems: 'center', marginBottom: 5 }}>
                <FlatList
                    data={thursday}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.content}>
                                <Text style={{ fontWeight: 'bold', marginTop: 6, marginBottom: 5 }} numberOfLines={1}>{item.title}</Text>
                                <Image style={{ height: 170, width: 120, marginBottom: 5 }} source={{ uri: item.image_url }} />
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Type : </Text>
                                    <Text style={styles.descText}>{item.type}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Episode : </Text>
                                    <Text style={styles.descText}>{item.episodes}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Airing : </Text>
                                    <Text style={styles.descText}>{Moment(item.airing_start).format('d MMM')}</Text>
                                </View>
                                <Button
                                    onPress={() => navigation.navigate('AnimeDetail', { animeID: item.mal_id })}
                                    title="Detail"
                                />
                            </View>
                        )
                    }}
                />
            </View>
        )
    }

    const Friday = () => {
        return (
            <View style={{ alignItems: 'center', marginBottom: 5 }}>
                <FlatList
                    data={friday}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.content}>
                                <Text style={{ fontWeight: 'bold', marginTop: 6, marginBottom: 5 }} numberOfLines={1}>{item.title}</Text>
                                <Image style={{ height: 170, width: 120, marginBottom: 5 }} source={{ uri: item.image_url }} />
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Type : </Text>
                                    <Text style={styles.descText}>{item.type}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Episode : </Text>
                                    <Text style={styles.descText}>{item.episodes}</Text>
                                </View>
                                <View style={styles.miniLayout}>
                                    <Text style={styles.descTitle}>Airing : </Text>
                                    <Text style={styles.descText}>{Moment(item.airing_start).format('d MMM')}</Text>
                                </View>
                                <Button
                                    onPress={() => navigation.navigate('AnimeDetail', { id: item.mal_id })}
                                    title="Detail"
                                />
                            </View>
                        )
                    }}
                />
            </View>
        )
    }

    const renderScene = SceneMap({
        1: Monday,
        2: Tuesday,
        3: Wednesday,
        4: Thursday,
        5: Friday
    });

    if (monday.length == 0 || tuesday.length == 0 || wednesday.length == 0) {
        return (
            <View style={styles.container}>
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <Text style={{ fontWeight: 'bold', fontSize: 24, marginTop: 6, marginBottom: 5, alignSelf: 'center' }}>Anime Schedule</Text>
                <TabView
                    navigationState={{ index, routes }}
                    renderScene={renderScene}
                    onIndexChange={setIndex}
                    initialLayout={{ width: Dimensions.get('window').width }}
                />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        width: 175,
        height: 310,
        margin: 6,
        borderWidth: 1,
        alignItems: 'center',
        borderRadius: 5,
        borderColor: 'grey',
    },
    miniLayout: {
        flexDirection: 'row',
    },
    descTitle: {
        marginBottom: 2,
        fontWeight: 'bold'
    },
    descText: {
        maxWidth: 150,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})



