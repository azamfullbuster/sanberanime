import axios from 'axios';
import React, { useState, useEffect, useContext } from 'react';
import { ActivityIndicator, FlatList, StyleSheet, View, Text, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { UserContext } from './Context/userContext'

export default function Home({ navigation }) {

    const [user,] = useContext(UserContext)
    const [topAnime, setTopAnime] = useState([]);
    const [topManga, setTopManga] = useState([]);

    const getTopAnime = () => {
        axios.get('https://api.jikan.moe/v3/top/anime/')
            .then(res => {
                const data1 = (res.data.top)
                console.log('res: ', data1)
                setTopAnime(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    const getTopManga = () => {
        axios.get('https://api.jikan.moe/v3/top/manga/')
            .then(res => {
                const data1 = (res.data.top)
                console.log('res: ', data1)
                setTopManga(data1)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    useEffect(() => {
        getTopAnime()
        getTopManga()
    }, [])

    if (topAnime.length == 0 || topManga.length == 0) {
        return (
            <View style={styles.container}>
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <ImageBackground source={{ uri: 'https://wallpaperaccess.com/full/1245781.jpg' }} style={{ width: Dimensions.get('window').width, height: 225 }}>
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', paddingVertical: 5 }}>
                            <Text style={styles.jumbotron}>Hello {user}</Text>
                            <Text style={styles.jumbotron}>Welcome to SanberAnime</Text>
                        </View>
                    </ImageBackground>
                    <View style={{ paddingVertical: 5 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Top Anime</Text>
                        <View style={styles.contentBox}>
                            {topAnime.length > 0 && (
                                <FlatList
                                    horizontal
                                    data={topAnime}
                                    keyExtractor={(item, index) => `${item.id}-${index}`}
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity style={styles.boxItem} onPress={() => navigation.navigate('AnimeDetail', { itemID: item.mal_id })}>
                                                <Image style={{ height: 65, width: 65, borderRadius: 10, marginBottom: 2 }} source={{ uri: item.image_url }} />
                                                <Text style={{ fontWeight: 'bold', color: 'black', marginBottom: 3, maxWidth: 130 }} numberOfLines={1}>{item.title}</Text>
                                                <Text style={{ fontSize: 10, color: 'black' }}>Members : {item.members}</Text>
                                                <Text style={{ fontSize: 10, color: 'black' }}>Score : {item.score}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            )}
                        </View>
                    </View>
                    <View style={{ paddingVertical: 5 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Top Manga</Text>
                        <View style={styles.contentBox}>
                            {topManga.length > 0 && (
                                <FlatList
                                    horizontal
                                    data={topManga}
                                    keyExtractor={(item, index) => `${item.id}-${index}`}
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity style={styles.boxItem} onPress={() => navigation.navigate('MangaDetail', { itemID: item.mal_id })}>
                                                <Image style={{ height: 65, width: 65, borderRadius: 10, marginBottom: 2 }} source={{ uri: item.image_url }} />
                                                <Text style={{ fontWeight: 'bold', color: 'black', marginBottom: 3, maxWidth: 130 }} numberOfLines={1}>{item.title}</Text>
                                                <Text style={{ fontSize: 10, color: 'black' }}>Members : {item.members}</Text>
                                                <Text style={{ fontSize: 10, color: 'black' }}>Score : {item.score}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            )}
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    jumbotron: {
        fontSize: 16,
        textDecorationStyle: 'double',
        textDecorationColor: 'white',
        fontWeight: 'bold',
        color: 'black',
        shadowColor: 'white'
    },
    contentBox: {
        flexDirection: 'row',
        backgroundColor: '#ead4ff',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#d4e4ff'
    },
    boxItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})