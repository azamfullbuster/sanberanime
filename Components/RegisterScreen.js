import React, { useState } from 'react'
import { Button, StyleSheet, Text, View, Alert, Image } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

export default function Register({ navigation }) {
    const firebaseConfig = {
        apiKey: "AIzaSyDtWVcognCljpBAXWEX2pqoOVb_vU4Q1CY",
        authDomain: "authenticationbootcamprn.firebaseapp.com",
        projectId: "authenticationbootcamprn",
        storageBucket: "authenticationbootcamprn.appspot.com",
        messagingSenderId: "488145610527",
        appId: "1:488145610527:web:55112c5671d421d92ef22a"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const submit = () => {
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(() => {
                console.log('Register Berhasil');
                navigation.navigate('LoginScreen')
                Alert.alert(
                    'Register Success !',
                    'Enjoy Your First Login ',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel'
                        },
                        { text: 'OK', onPress: () => console.log('OK Pressed') }
                    ],
                    { cancelable: true }
                )
            }).catch(() => {
                console.log("register gagal")
                Alert.alert(
                    'Register Failed !',
                    'Please try to register again ',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel'
                        },
                        { text: 'OK', onPress: () => console.log('OK Pressed') }
                    ],
                    { cancelable: true }
                )
            })
    }
    return (
        <View style={styles.container}>
            <Image
                style={styles.imageLogo}
                source={require('./assets/logo.png')}
            />
            <Text style={styles.pageTitle}>REGISTER</Text>
            <TextInput
                style={styles.input}
                placeholder="Input Email"
                value={email}
                onChangeText={(value) => setEmail(value)}

            />
            <TextInput
                style={styles.input}
                placeholder="Input Password"
                value={password}
                onChangeText={(value) => setPassword(value)}
            />
            <Button onPress={submit} title="REGISTER" />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    imageLogo: {
        justifyContent: 'center',
        alignSelf: 'center',
        height: 78,
        width: 290
    },
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 40,
        fontSize: 25,
        fontWeight: 'bold'
    },
    input: {
        borderWidth: 1,
        borderColor: 'grey',
        paddingHorizontal: 10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    }
})
