import React, { useState, useContext } from 'react';
import { Image, StyleSheet, View, Text, Alert, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { UserContext } from './Context/userContext'
import * as firebase from 'firebase';

export default function LoginScreen({ navigation }) {
    const firebaseConfig = {
        apiKey: "AIzaSyDtWVcognCljpBAXWEX2pqoOVb_vU4Q1CY",
        authDomain: "authenticationbootcamprn.firebaseapp.com",
        projectId: "authenticationbootcamprn",
        storageBucket: "authenticationbootcamprn.appspot.com",
        messagingSenderId: "488145610527",
        appId: "1:488145610527:web:55112c5671d421d92ef22a"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    const [, setUser] = useContext(UserContext)

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const submit = () => {
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().signInWithEmailAndPassword(email, password).then(() => {
            console.log("berhasil login");
            setUser(email)
            navigation.navigate("LoginSplash")
            Alert.alert(
                'Login Success !',
                'Welcome to the Anime Family',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                    },
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: true }
            )
        }).catch(() => {
            console.log("Login gagal")
            Alert.alert(
                'Login Failed !',
                'Email / Username / Password Incorrect',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                    },
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: true }
            )
        })
    }

    return (
        <View style={styles.container}>
            <ScrollView>
                <Image
                    style={styles.imageLogo}
                    source={require('./assets/logo.png')}
                />
                <Text style={styles.pageTitle}>LOGIN</Text>
                <View style={styles.form}>
                    <Text>Email</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Input Email"
                        value={email}
                        onChangeText={(value) => setEmail(value)}

                    />
                    <Text>Password</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Masukin Password"
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />
                </View>
                <View style={styles.actionZone}>
                    <TouchableOpacity
                        style={styles.button}
                        onPressIn={submit}
                    >
                        <Text style={{ fontWeight: 'bold', color: 'white' }}>Login</Text>
                    </TouchableOpacity>
                    <Text>Don't Have Account ? </Text>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("RegisterScreen")}
                    >
                        <Text style={{ color: 'blue' }}>Create Account</Text>
                    </TouchableOpacity>
                    <Text>Or Continue With MyAnimeList Username</Text>
                    <TouchableOpacity
                        style={styles.buttonGoogle}
                        onPressIn={() => navigation.navigate('UsernameLogin')}
                    >
                        <Ionicons name="md-happy" size={30} color="black" />
                        <Text style={{ fontWeight: 'bold', color: 'black' }}>MyAnimeList</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    imageLogo: {
        justifyContent: 'center',
        alignSelf: 'center',
        height: 78,
        width: 290
    },
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 40,
        fontSize: 25,
        fontWeight: 'bold'
    },
    form: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: 24,
        paddingVertical: 24,
    },
    input: {
        borderWidth: 1,
        borderColor: 'grey',
        paddingHorizontal: 10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    },
    actionZone: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: 24,
    },
    button: {
        flexDirection: 'row',
        backgroundColor: 'darkblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    },
    buttonGoogle: {
        flexDirection: 'row',
        backgroundColor: 'lightblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    }
})