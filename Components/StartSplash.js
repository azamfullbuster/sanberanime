import React, { useState, useEffect } from 'react';
import { View, Animated, Easing } from 'react-native';


const Splash = ({ navigation }) => {

    const [spinAnim, setSpinAnim] = useState(new Animated.Value(0));
    const spin = spinAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg'],
    });

    useEffect(() => {
        Animated.loop(
            Animated.timing(spinAnim, {
                toValue: 1,
                duration: 3000,
                easing: Easing.linear,
                useNativeDriver: true,
            }),
        ).start();

        setTimeout(() => {
            navigation.navigate("LoginScreen")
        }, 4000)
    }, [])

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Animated.Image
                style={{ height: 100, width: 100, transform: [{ rotate: spin }] }}
                source={{
                    uri:
                        'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png',
                }}
            />
        </View>
    )
}

export default Splash
