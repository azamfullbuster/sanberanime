import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Dimensions, StyleSheet, View, Text, TouchableOpacity, FlatList, Image, ActivityIndicator } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

export default function MangaDetail({ route, navigation }) {
    const { itemID } = route.params;
    const [manga, setManga] = useState({});
    const [genre, setGenre] = useState([]);

    const getManga = (id) => {
        axios.get(`https://api.jikan.moe/v3/manga/${id}/`)
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1)
                setManga(data1)
                setGenre(data1.genres)
            }).catch(err => {
                console.log('error: ', err)
            })
    }

    useEffect(() => {
        getManga(itemID)
    }, [])

    if (Object.keys(manga).length === 0) {
        return (
            <View style={styles.container}>
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={{ fontWeight: 'bold', fontSize: 24, alignSelf: 'center' }}>
                        {manga.title}
                    </Text>
                    <View style={styles.mainLayout}>
                        <View style={styles.leftDesc}>
                            <Image style={{ height: 255, width: 180, marginBottom: 5 }} source={{ uri: manga.image_url }} />
                        </View>
                        <View style={styles.rightDesc}>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Japanese Title : </Text>
                                <Text style={styles.descText}>{manga.title_japanese}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Type : </Text>
                                <Text style={styles.descText}>{manga.type}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Volumes : </Text>
                                <Text style={styles.descText}>{manga.volumes}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Chapters : </Text>
                                <Text style={styles.descText}>{manga.chapters}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Status : </Text>
                                <Text style={styles.descText}>{manga.status}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Rating : </Text>
                                <Text style={styles.descText}>{manga.rating}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Score : </Text>
                                <Text style={styles.descText}>{manga.score}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Members : </Text>
                                <Text style={styles.descText}>{manga.members}</Text>
                            </View>
                            <View style={styles.miniLayout}>
                                <Text style={styles.descTitle}>Favorites : </Text>
                                <Text style={styles.descText}>{manga.favorites}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ marginHorizontal: 5, marginBottom: 5 }}>
                        <Text style={{ fontWeight: 'bold', marginBottom: 3 }}>Genre : </Text>
                        <FlatList
                            horizontal
                            data={genre}
                            keyExtractor={(item, index) => `${item.id}-${index}`}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableOpacity style={styles.boxItem}>
                                        <Text style={{
                                            color: 'black', borderWidth: 1, borderColor: 'black'
                                        }}>{item.name}</Text>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                        <Text style={styles.descTitle}>Synopsis</Text>
                        <Text style={{ maxWidth: Dimensions.get('window').width, textAlign: 'justify' }}>{manga.synopsis}</Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    mainLayout: {
        flexDirection: 'row',
        paddingVertical: 5,
        borderTopColor: 'black',
        borderTopWidth: 1
    },
    miniLayout: {
        flexDirection: 'row',
        paddingVertical: 2
    },
    leftDesc: {
        flexDirection: 'column',
        padding: 5
    },
    rightDesc: {
        flexDirection: 'column',
        paddingLeft: 5
    },
    descTitle: {
        marginBottom: 5,
        fontWeight: 'bold'
    },
    descText: {
        maxWidth: 150,
    },
    boxItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
        marginBottom: 5
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})